import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import {ForgotPas} from "./forgotPas/ForgotPas";
import {Login} from "./Login/Login";
import {UserConf} from "./userConfig/userConf";
import {UsersPage} from "./usersPage/usersPage";

const App = () => (
    <div className="app-routes">
        <Switch>
            <Route path="/login" component={Login} />
            <Route path="/forgot" component={ForgotPas} />
            <Route path="/users" component={UsersPage} />
            <Route path="/user_config" component={UserConf} />
        </Switch>
    </div>
);

export default App;