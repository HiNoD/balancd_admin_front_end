import * as React from 'react';
import {Redirect} from "react-router";
import './Login.css';

export class Login extends React.Component
{

    public Logged = () =>
    {
        this.setState({redirect: true});
    }

    public render()
    {
        if (this.state)
        {
            return <Redirect to="/users" />;
        }
        return(
            <div className={"Login"}>
                <p className={"LoginBox"}>
                    <p><input placeholder={"Email"} className={"EmailBox"} type={"email"}/></p>
                    <p><input placeholder={"Password"} className={"PassBox"} type={"password"}/></p>
                    <p><button className={"Login_but"} type="button" onClick={this.Logged}>Login</button></p>
                    <p><a href={"./forgot"}>Forgot password?</a></p>
                </p>
            </div>
        );
    }
}


